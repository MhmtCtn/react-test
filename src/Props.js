import React from 'react';

class Props extends React.Component {
    render() {
        let text = this.props.txt
        return <h1>{text}</h1>
    }
}

Props.propTypes = {
    txt: React.PropTypes.string,
    num: React.PropTypes.number.isRequired
}

Props.defaultProps = {
    txt: "this is a default prop text"
}

export default Props