import React from 'react';

class State extends React.Component {
    constructor() {
        super();
        this.state = {
            txt: 'this is the state text',
            num: 0
        }
    }

    update(e) {
        this.setState({txt: e.target.value})
    }
    
    render() {
        return(
            <div>
                <input type="text"
                    onChange={this.update.bind(this)} />
                <h1>{this.state.txt} - {this.state.num}</h1>
            </div>
        )
    }
}

export default State