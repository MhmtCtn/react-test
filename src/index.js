import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import App2 from './App2';
import App3 from './App3';
import Props from './Props';
import State from './State';

ReactDOM.render(
  <div>
    <App />
    <App2 />
    <App3 />
    <Props txt='this is the prop text' num={5} />
    <State />
  </div>,
  document.getElementById('root')
);
